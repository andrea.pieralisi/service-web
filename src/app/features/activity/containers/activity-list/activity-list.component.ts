import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Activity } from 'src/app/core/models/activity.models';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ActivityModuleState } from 'src/app/core/store/activity/activity.state';
import * as ActivitySelectors from '../../../../core/store/activity/activity.selectors';
import * as AcitivityActions from '../../../../core/store/activity/activity.actions';

@Component({
  selector: 's-web-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.scss']
})
export class ActivityListComponent implements OnInit {

  
  activities$: Observable<Activity[]>;
  loading$: Observable<boolean>;

  displayedColumns: string[] = ['code', 'customer', 'description'];

  constructor(private store: Store<ActivityModuleState>, 
              private router: Router, 
              private route: ActivatedRoute) { 
    this.activities$ = this.store.select( ActivitySelectors.selectActivities);
    this.loading$ = this.store.select(ActivitySelectors.selectLoading)
                      .pipe(switchMap( l => of(l.loading)));
     
  }

  ngOnInit(): void {
   
    this.store.dispatch(AcitivityActions.getActivities())
  }

  /*
  addActivity() {
    let activity: Activity = {id: this.id, description: this.description};
    this.store.dispatch(AcitivityActions.addActivity({payload: activity}));
  }*/

  newActivity(){
    this.store.dispatch(AcitivityActions.resetOperactionSuccess());
    this.router.navigate(['activity/new']);
    
  }

  selectRow({id, code, description}: any) {
    let activity: Activity = {id, code, description}
    //this.store.dispatch(AcitivityActions.setCurrentActivity({payload: activity}));
    this.router.navigate(['activity/update/', activity.id]);
  }

}


