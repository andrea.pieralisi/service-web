import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { debounceTime, finalize, switchMap, tap } from 'rxjs/operators';
import { ActivityApiService } from 'src/app/core/api/activity-api.service';
import { CustomerApiService } from 'src/app/core/api/customer-api.service';
import { Activity } from 'src/app/core/models/activity.models';
import { Customer } from 'src/app/core/models/customer.models';
import { ActivityModuleState } from 'src/app/core/store/activity/activity.state';
import * as AcitivityActions from '../../../../core/store/activity/activity.actions';
import * as ActivitySelectors from '../../../../core/store/activity/activity.selectors';

@Component({
  selector: 's-web-activity-new',
  templateUrl: './activity-new.component.html',
  styleUrls: ['./activity-new.component.scss']
})
export class ActivityNewComponent implements OnInit {
  createdId: number = -1;
  code: string = '';
  description: string = '';
  loading$: Observable<boolean>;


  searchCustomerCtrl = new FormControl();
  filteredCustomers: any;
  isLoading = false;
  errorMsg: string = '';

  constructor(private store: Store<ActivityModuleState>,
    private router: Router,
    private route: ActivatedRoute,
    private customerService: CustomerApiService) {
    
    this.loading$ = this.store.select(ActivitySelectors.selectLoading)
      .pipe(switchMap(l => of(l.loading)));

    this.store.select(ActivitySelectors.currentActivity)  
      .subscribe( result => {
         if (result){ this.createdId = (result && result.id)? result.id : this.createdId }
      });
    
    this.store.select(ActivitySelectors.operationSuccess)  
      .subscribe( result => {
         if (result && this.createdId ){ this.router.navigate(['activity/update', this.createdId])}
      });
  }

  ngOnInit(): void {
    this.searchCustomerCtrl.valueChanges
      .pipe(
        debounceTime(500),
        tap(() => {
          this.errorMsg = "";
          this.filteredCustomers = [];
          this.isLoading = true;
        }),
        switchMap(value => this.customerService.getCustomers()
        .pipe(
            finalize(() => {
              this.isLoading = false
            }),
          )
        )
      )
      .subscribe(data => {
        if (data == undefined) {
          this.errorMsg = 'Error';
          this.filteredCustomers = [];
        } else {
          this.errorMsg = "";
          this.filteredCustomers = data;
        }

        console.log(this.filteredCustomers);
      });
  }



  addActivity() {
    let activity: Activity = { code: this.code, description: this.description, customer: this.searchCustomerCtrl.value? this.searchCustomerCtrl.value.Name : ''};
    this.store.dispatch(AcitivityActions.addActivity({ payload: activity }));
  }

  displayFn(value: Customer) {
    return '(' + (value && value.id? value.id : 'noID') + ')' + (value && value.Name ? value.Name : '');

  }
  

}
