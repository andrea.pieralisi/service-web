import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Activity } from 'src/app/core/models/activity.models';
import { ActivityModuleState } from 'src/app/core/store/activity/activity.state';
import * as AcitivityActions from '../../../../core/store/activity/activity.actions';
import * as ActivitySelectors from '../../../../core/store/activity/activity.selectors';

@Component({
  selector: 's-web-activity-update',
  templateUrl: './activity-update.component.html',
  styleUrls: ['./activity-update.component.scss']
})
export class ActivityUpdateComponent implements OnInit {
  id: number = 0;
  code: string = '';
  description: string = '';
  customer: string = '';

  loading$: Observable<boolean>;
  private sub: any;
  
  get loadingText() {
    return !this.id ? 'Loading' : 'Saving';
  }

  constructor(private store: Store<ActivityModuleState>,
    private router: Router,
    private route: ActivatedRoute) { 

      this.loading$ = this.store.select(ActivitySelectors.selectLoading)
      .pipe(switchMap(l => of(l.loading))); 

    this.store.select(ActivitySelectors.currentActivity)
                  .subscribe(cActivity => {
                      if (cActivity) {
                        cActivity.id ? this.id = cActivity.id : this.id = 0;
                        this.code = cActivity.code;
                        this.description = cActivity.description;
                        this.customer = cActivity.customer?cActivity.customer:'';
                      }
                  });
    
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      let _id = +params['id']; // (+) converts string 'id' to a number
      this.store.dispatch(AcitivityActions.getActivity({payload: _id}));
      // In a real app: dispatch action to load the details here.
   });
  }

  updateActivity(){
    this.store.dispatch(AcitivityActions.updateActivity({ payload: {id: this.id, code: this.code, description: this.description}} ));
  }
  back(){
    this.router.navigate(['activity']);
  }

}
