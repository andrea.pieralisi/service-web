import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivityRoutingModule } from './activity-routing.module';
import { Store, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../../../environments/environment';
import * as ActivityReducers from '../../core/store/activity/activity.reducers';
import { SharedModule } from 'src/app/shared/shared.module';
import { EffectsModule } from '@ngrx/effects';
import { ActivityEffects } from '../../core/store/activity/activity.effects';
import { ActivityListComponent } from './containers/activity-list/activity-list.component';
import { ActivityNewComponent } from './containers/activity-new/activity-new.component';
import { ActivityModuleState } from 'src/app/core/store/activity/activity.state';
import * as AcitivityActions from '../../core/store/activity/activity.actions';
import { ActivityUpdateComponent } from './containers/activity-update/activity-update.component';


@NgModule({
  declarations: [ActivityListComponent, ActivityNewComponent, ActivityUpdateComponent],
  imports: [
    SharedModule,
    ActivityRoutingModule,
    StoreModule.forFeature( ActivityReducers.activityFeaturekey,  ActivityReducers.reducer),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    EffectsModule.forFeature([ActivityEffects])
  ]
})
export class ActivityModule { 
  constructor(private store: Store<ActivityModuleState>, ){
    
  }
}
