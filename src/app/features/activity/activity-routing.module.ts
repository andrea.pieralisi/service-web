import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActivityListComponent } from './containers/activity-list/activity-list.component';
import { ActivityNewComponent } from './containers/activity-new/activity-new.component';
import { ActivityUpdateComponent } from './containers/activity-update/activity-update.component';

const routes: Routes = [
  { path: '', component: ActivityListComponent },
  { path: 'new', component: ActivityNewComponent },
  { path: 'update/:id', component: ActivityUpdateComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivityRoutingModule { }
