import { state } from "@angular/animations";
import { Action, createReducer, on } from "@ngrx/store";
import * as ActivityActions from "./activity.actions";
import { ActivityState } from "./activity.state";



const initialState: ActivityState = {
    activities: [],
    loading: false,
    error: ''
};



 /*

export function ActivityReducer(state: ActivityState = initialState, action: ActivityAction) {
    switch (action.type) {

        case ActivityActionTypes.GET_ACTIVITIES:
            return {
                ...state,
                loading: true
            };
        case ActivityActionTypes.GET_ACTIVITIES_SUCCESS:
            return {
                ...state,
                activities: action.payload,
                loading: false
            }
        case ActivityActionTypes.GET_ACTIVITIES_FAIL:
            return {
                ...state,
                error: action.payload,
                loading: false
            };
        case ActivityActionTypes.ADD_ACTIVITY:
            return {
                ...state,
                loading: true
            };
        case ActivityActionTypes.ADD_ACTIVITY_SUCCESS:
            return {
                ...state,
                activities: [action.payload, ...state.activities],
                loading: false
            };

        default:
            return state;
    }
}

*/
export const activityFeaturekey = 'activity';

const activityReducer = createReducer(
    initialState,
    on(ActivityActions.resetOperactionSuccess, state => ({ ...state, loading: false, operationSuccess: false, error: '' })),
    on(ActivityActions.getActivities, state => ({ ...state, activities: [], current: undefined, loading: true })),
    on(ActivityActions.getActivitiesSuccess, (state, {payload}) => ({  ...state, activities: payload, loading: false })),
    on(ActivityActions.getActivitiesFail,    (state, {payload}) => ({ ...state, error: payload, loading: false })),

    on(ActivityActions.getActivity, state => ({ ...state, current: undefined, loading: true })),
    on(ActivityActions.getActivitySuccess, (state, {payload}) => ({  ...state, current: payload, loading: false })),
    on(ActivityActions.getActivityFail,    (state, {payload}) => ({ ...state, error: payload, loading: false })),
    
    on(ActivityActions.addActivity, (state, {payload}) => ({...state, operationSuccess: false, current: undefined, loading: true})),
    on(ActivityActions.addActivitySuccess, (state, {payload}) => ({...state,  activities: [payload, ...state.activities] , current: payload, operationSuccess: true, loading: false})),
    on(ActivityActions.addActivityFail, (state, {payload}) => ({ ...state, error: payload, loading: false })),
    
    on(ActivityActions.setCurrentActivity, (state, {payload}) => ({...state, operationSuccess: false, current: payload, loading: false})),

    on(ActivityActions.updateActivity, (state, {payload}) => ({...state, operationSuccess: false, current: undefined, loading: true})),
    on(ActivityActions.updateActivitySuccess, (state, {payload}) => {
       return { ...state,  
                current: undefined, 
                operationSuccess: true, 
                loading: false
              }
    }),
    on(ActivityActions.updateActivityFail, (state, {payload}) => ({ ...state, error: payload, loading: false }))
  );
  
  export function reducer(state: ActivityState | undefined, action: Action) {
    return activityReducer(state, action);
  }