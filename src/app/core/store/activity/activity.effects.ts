import { Injectable } from '@angular/core';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { map, mergeMap, catchError, delay } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
//import { ActivityActionTypes, AddActivitiyAction, AddActivitiyFailAction, AddActivitiySuccessAction, GetActivitiesAction, GetActivitiesFailAction, GetActivitiesSuccessAction } from './activity.actions';
import * as ActivityActions from './activity.actions';
import { Activity } from '../../models/activity.models';
import { ActivityApiService } from '../../api/activity-api.service';




/*
@Injectable()
export class ActivityEffects {
    @Effect() getActivities$ = this.actions$
        .pipe(
            ofType<GetActivitiesAction>(ActivityActionTypes.GET_ACTIVITIES),
            mergeMap(
                () => of(fakeActivities)
                    .pipe(
                        delay(2000),
                        map(data => {
                            return new GetActivitiesSuccessAction(data)
                        }),
                        catchError(error => of(new GetActivitiesFailAction(error)))
                    )
            )
        )

    @Effect() addActivity$ = this.actions$
        .pipe(
            ofType<AddActivitiyAction>(ActivityActionTypes.ADD_ACTIVITY),
            mergeMap(
                (data) => of(data.payload)
                    .pipe(
                        delay(2000),
                        map(data => {
                            return new AddActivitiySuccessAction(data)
                        }),
                        catchError(error => of(new AddActivitiyFailAction(error)))
                    )
            )
        )

    constructor(
        private actions$: Actions,
    ) { }
}
*/
@Injectable()
export class ActivityEffects {
    
    getActivities$ = createEffect(() => this.actions$.pipe(
        ofType(ActivityActions.getActivities),
        mergeMap(
            () => this.service.getActivites()
                .pipe(
                    map(payload => ActivityActions.getActivitiesSuccess({ payload })),
                    catchError(payload => of(ActivityActions.addActivityFail({ payload }))
                    )
                )
        )));

    addActivity$ = createEffect(() => this.actions$
        .pipe(
            ofType(ActivityActions.addActivity),
            mergeMap(
                (data) => this.service.addActivity(data.payload)
                    .pipe(
                        delay(2000),
                        map(payload => ActivityActions.addActivitySuccess({ payload })),
                        catchError(error => of(ActivityActions.addActivityFail(error)))
                    )
            )
        ));

    updateActivity$ = createEffect(() => this.actions$
        .pipe(
            ofType(ActivityActions.updateActivity),
            mergeMap(
                (data) => this.service.updateActivity(data.payload)
                    .pipe(
                        delay(2000),
                        map(payload => ActivityActions.updateActivitySuccess({ payload })),
                        catchError(error => of(ActivityActions.updateActivityFail(error)))
                    )
            )
        ));

    getActivity$ = createEffect(() => this.actions$
    .pipe(
        ofType(ActivityActions.getActivity),
        mergeMap(
            (data) => this.service.getActivity(data.payload)
                .pipe(
                    delay(2000),
                    map(payload => ActivityActions.getActivitySuccess({ payload })),
                    catchError(error => of(ActivityActions.getActivityFail(error)))
                )
        )
    ));

    constructor(
        private actions$: Actions,
        private service: ActivityApiService
    ) { }


}