import { createSelector } from "@ngrx/store";
import { ActivityModuleState, ActivityState } from "./activity.state";

export const selectActivity = (state: ActivityModuleState) => state.activity;

export const selectActivities = createSelector(
    selectActivity,
    (state: ActivityState) => state.activities
);

export const selectLoading = createSelector(
    selectActivity,
    (state: ActivityState) => ({loading: state.loading, error: state.error})
)


export const operationSuccess = createSelector(
    selectActivity,
    (state: ActivityState) => state.operationSuccess
)


export const currentActivity = createSelector(
    selectActivity,
    (state: ActivityState) => state.current
)