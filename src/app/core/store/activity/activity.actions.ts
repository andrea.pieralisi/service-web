import { Action, createAction, props } from '@ngrx/store';
import { Activity } from '../../models/activity.models';


export const getActivities = createAction('[ACTIVITY] get Activities');
export const getActivitiesSuccess = createAction('[ACTIVITY] get Activities Success', props<{payload: Activity[]}>());
export const getActivitiesFail = createAction('[ACTIVITY] get Activities Fail', props<{ payload: any}>());

export const getActivity = createAction('[ACTIVITY] get Activity', props<{payload: number}>());
export const getActivitySuccess = createAction('[ACTIVITY] get Activity Success', props<{payload: Activity}>());
export const getActivityFail = createAction('[ACTIVITY] get Activity Fail', props<{ payload: any}>());

export const addActivity = createAction('[ACTIVITY] add Activity', props<{payload: Activity}>());
export const addActivitySuccess = createAction('[ACTIVITY] add Activity Success', props<{payload: Activity}>());
export const addActivityFail = createAction('[ACTIVITY] add Activity Fail', props<{ payload: any}>());

export const setCurrentActivity = createAction('[ACTIVITY] setCurrent Activity', props<{payload: Activity}>());

export const updateActivity = createAction('[ACTIVITY] update Activity', props<{payload: Activity}>());
export const updateActivitySuccess = createAction('[ACTIVITY] update Activity Success', props<{payload: Activity}>());
export const updateActivityFail = createAction('[ACTIVITY] update Activity Fail', props<{ payload: any}>());

export const resetOperactionSuccess = createAction('[ACTIVITY] reset operation success');

/*
export enum ActivityActionTypes {
    GET_ACTIVITIES = '[ACTIVITY] Get Activity',
    GET_ACTIVITIES_SUCCESS = '[ACTIVITY] Get Posts Success',
    GET_ACTIVITIES_FAIL = '[ACTIVITY] Get Posts Fail',
    
    ADD_ACTIVITY = '[ACTIVITY] Add Activity',
    ADD_ACTIVITY_SUCCESS = '[ACTIVITY] Add Activity Success',
    ADD_ACTIVITY_FAIL = '[ACTIVITY] Add Activity Fail'
}


export class GetActivitiesAction implements Action {
    readonly type = ActivityActionTypes.GET_ACTIVITIES;
}

export class GetActivitiesSuccessAction implements Action {
    readonly type = ActivityActionTypes.GET_ACTIVITIES_SUCCESS;
    constructor(public payload: Activity[]){}
}

export class GetActivitiesFailAction implements Action {
    readonly type = ActivityActionTypes.GET_ACTIVITIES_FAIL;
    constructor(public payload: any ) {}
}


export class AddActivitiyAction implements Action {
    readonly type = ActivityActionTypes.ADD_ACTIVITY;
    constructor(public payload: Activity ) {}
}

export class AddActivitiySuccessAction implements Action {
    readonly type = ActivityActionTypes.ADD_ACTIVITY_SUCCESS;
    constructor(public payload: Activity ) {}
}

export class AddActivitiyFailAction implements Action {
    readonly type = ActivityActionTypes.ADD_ACTIVITY_FAIL;
    constructor(public payload: any ) {}
}


export type ActivityAction = 
   GetActivitiesAction |
   GetActivitiesSuccessAction |
   GetActivitiesFailAction |
   AddActivitiyAction |
   AddActivitiySuccessAction | 
   AddActivitiyFailAction;
*/

