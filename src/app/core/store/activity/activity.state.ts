import { Activity } from "../../models/activity.models";

export interface ActivityModuleState {
    activity: ActivityState;
}

export interface ActivityState {
    activities: Activity[];
    current?: Activity;
    loading: boolean;
    operationSuccess?: boolean;
    error: ''
}