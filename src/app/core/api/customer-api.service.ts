import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of, throwError } from "rxjs";
import { delay } from "rxjs/operators";
import { Activity } from "../models/activity.models";
import { Customer } from "../models/customer.models";


const fakeCustomers: Customer[] = [{ id: 1, Name: 'Andrea'}, {id: 2, Name: 'Giacomo'}];

@Injectable({
    providedIn: 'root'
  })
  export class CustomerApiService{

    private customers: Customer[] = Object.assign([], fakeCustomers);

    constructor( private http: HttpClient) {}

    getCustomers(): Observable<Customer[]>{
      return of(this.customers).pipe(delay(1000));
    }

  }