import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of, throwError } from "rxjs";
import { delay } from "rxjs/operators";
import { Activity } from "../models/activity.models";


const fakeActivities: Activity[] = [{ id: 1, code:'act1', customer: 'Giacomo', description: 'activity1' }, 
                                    { id: 2, code: 'act2', customer: 'Andrea', description: 'activity2' }];

@Injectable({
    providedIn: 'root'
  })
  export class ActivityApiService{

    private activities: Activity[] = Object.assign([], fakeActivities);

    constructor( private http: HttpClient) {}

    getActivites(): Observable<Activity[]>{
      return of(this.activities).pipe(delay(1000));
    }

    getActivity(id: number): Observable<Activity>{
      let activity = this.activities.find(a => a.id === id);
      if (activity === undefined) throw new Error('Activiy not found');
      return of(activity).pipe(delay(500));
    }

    addActivity(activity: Activity): Observable<Activity> {
      let newAct = {...activity, id: Math.floor(Math.random()*100000000)};
      this.activities = [...this.activities, newAct];
      return of(newAct).pipe(delay(500));
    }

    updateActivity(activity: Activity): Observable<Activity> {
      this.activities = Object.assign([],[...this.activities.filter( a => a.id != activity.id), activity]);
      return of(activity).pipe(delay(500));
    }
  }