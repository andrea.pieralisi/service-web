export interface Activity {
    id?: number;
    code: string;
    description: string;
    customer?: string;
    products?: Product[];
}

export interface Product {
    id: number;
    code: string;
    description: string;
    model: string;
    family: string;
}

