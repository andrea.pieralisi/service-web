import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule} from '@angular/material/toolbar';
import { MatButtonModule} from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';
import { RouterModule } from '@angular/router';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCardModule} from '@angular/material/card';
import {MatAutocompleteModule} from '@angular/material/autocomplete';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
    //Material 
    MatToolbarModule,
    MatButtonModule,
    MatTableModule,
    MatInputModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatAutocompleteModule,

    // forms
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    CommonModule,
    RouterModule,
    
    MatToolbarModule,
    MatButtonModule,
    MatTableModule,
    MatInputModule,
    MatIconModule,
    
    MatProgressSpinnerModule,
    MatCardModule,
    MatAutocompleteModule,

    FormsModule,
    ReactiveFormsModule
  ]
})
export class SharedModule { }
